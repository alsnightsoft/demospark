package edu.pucmm;

import edu.pucmm.model.RouteBase;
import edu.pucmm.routes.Data;
import edu.pucmm.routes.DataGet;
import edu.pucmm.routes.Principal;
import spark.Spark;

public class Demo {

    public static void main(String[] args) {
        Spark.port(8070);
        addRoute(new Principal());
        addRoute(new Data());
        addRoute(new DataGet());
        Spark.init();
    }

    private static void addRoute(RouteBase routeBase) {
        Spark.get(routeBase.path, routeBase);
    }
}
