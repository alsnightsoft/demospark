package edu.pucmm.model;

import spark.Route;

public abstract class RouteBase implements Route {

    public String path;

    protected RouteBase(String path) {
        this.path = path;
    }
}
