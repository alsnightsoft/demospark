package edu.pucmm.routes;

import edu.pucmm.model.RouteBase;
import spark.Request;
import spark.Response;

public class DataGet extends RouteBase {

    public DataGet() {
        super("/demo/:id");
    }

    @Override
    public Object handle(Request request, Response response) throws Exception {
        try {
            String ID = request.params(":id");
            if (ID != null && !ID.isEmpty()) {
                return Data.frutas().get(Integer.parseInt(ID));
            }
            return "No Param";
        } catch (Exception ignored) {
            return "ID no valido";
        }
    }
}
